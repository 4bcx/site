---
title: "about"
description: "About me and my circle"
---

- about me
    - my name is Ahmed Mohamed Alaa but I also go by
        - just علاء as my friends and colleagues call me
        - or بشمهندس أحمد علاء when people are being formal with me
        - or البرنس because I am برنس
    - *4bcx* doesn't mean anything. it's just the first available && shortest && not premium domain I found that I didn't hate
    - I think in lists and bullet-points, obvious
    - the tagline is *I break stuff to figure out how they work, or to repurpose them and make other stuff*
    - currently, I manage a [technology lab](https://web.facebook.com/EMEBORG/) for [ITIDA](https://itida.gov.eg/)
    - projects on [gitlab](https://gitlab.com/4bcx), [github](https://github.com/4bcx) and [sourcehut](https://sr.ht/~a2)
    - and my current [cv](/cv)

* socials \
I'm fighting my [FOMO](https://en.wikipedia.org/wiki/Fear_of_missing_out) and slowly moving away from social media, but you can find my:
    * old rants on [twitter](https://twitter.com/4b_cx), current rants on <a rel="me" href="https://masto.ai/@4bcx">mastodon</a>
    * pictures on [instagram](https://www.instagram.com/4b_cx)
    * few interactions with friends and family on [facebook](https://facebook.com/ahmed4bcx), and with coworkers on [linkedin](https://www.linkedin.com/in/ahmed4bcx)

+ contact
    + [email](mailto:aa@4b.cx): I usually respond with many [bullet-points](https://twitter.com/itsnashflynn/status/1440002336999583748)
    + or you can download my [vcard](/vcard), and [pgp key](/pgp)

- [my circle](/kin)
    - [my awesome wife](/kin#she-who-must-be-obeyed) and I made those
        - [Yahya](/kin#the-little-one)
        - [Yunus](/kin#the-littler-one)
    - [the bro](/kin#the-bro), the one and only, the most awesome, the cool uncle of my children

---

this website source code, licenses and attributions can be found [here](https://gitlab.com/4bcx/site/)
