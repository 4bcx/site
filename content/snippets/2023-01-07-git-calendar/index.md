---
title: "git calendar"
description: "GitHub/GitLab like contributions calendar for local repositories"
---

This is a script I wrote to render GitHub/GitLab like contributions calendar for local repositories, it should render something like [this](/images/git_calendar.svg)

{{ code(file="git_calendar.sh", highlight="bash") }}
