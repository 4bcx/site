---
title: "projects"
description: "List of projects and stuff"
template: "projects.html"

extra:
    projects:
    - name: "webring"
      description: "minimal webring for beginners like me"
      url: "https://wr.4b.cx/"
      sources:
      - name: "gitlab"
        url: "https://gitlab.com/4bcx/webring"
      - name: "github"
        url: "https://github.com/4bcx/webring"
      - name: "sourcehut"
        url: "https://git.sr.ht/~a2/webring"
      license:
        name: "MIT"
        url: "https://raw.githubusercontent.com/4bcx/webring/main/LICENSE"

    - name: "1k"
      description: "minimal cv, 1kB Club submission"
      url: "https://1k.4b.cx/"
      sources:
      - name: "sourcehut"
        url: "https://git.sr.ht/~a2/1k"
      license:
        name: "MIT"
        url: "https://git.sr.ht/~a2/1k/blob/main/LICENSE"

    - name: "site"
      description: "my personal website, most probably you're viewing it now"
      url: "https://4b.cx/"
      sources:
      - name: "gitlab"
        url: "https://gitlab.com/4bcx/site"
      license:
        name: "listed here"
        url: "https://gitlab.com/4bcx/4bcx/-/raw/main/LICENSE"

    - name: "no-style-please"
      description: "a (nearly) no-CSS, fast, minimalist Zola theme"
      url: "https://4bcx.gitlab.io/no-style-please/"
      sources:
      - name: "gitlab"
        url: "https://gitlab.com/4bcx/no-style-please"
      - name: "github"
        url: "https://github.com/4bcx/no-style-please"
      license:
        name: "MIT"
        url: "https://gitlab.com/4bcx/no-style-please/-/raw/main/LICENSE"

    - name: "xoproto"
      description: "a proto board that doubles as badge"
      sources:
      - name: "gitlab"
        url: "https://gitlab.com/4bcx/xoproto"
      - name: "github"
        url: "https://github.com/4bcx/xoproto"
      license:
        name: "TAPR OHL"
        url: "https://files.tapr.org/OHL/TAPR_Open_Hardware_License_v1.0.txt"

    - name: "imgg"
      description: "cover images generator"
      sources:
      - name: "gitlab"
        url: "https://gitlab.com/4bcx/imgg"
      - name: "github"
        url: "https://github.com/4bcx/imgg"
      license:
        name: "The Unlicense"
        url: "https://unlicense.org"

    - name: "dweb"
      description: "dumb static website builder"
      sources:
      - name: "gitlab"
        url: "https://gitlab.com/4bcx/dweb"
      - name: "github"
        url: "https://github.com/4bcx/dweb"
      license:
        name: "The Unlicense"
        url: "https://unlicense.org"

    - name: "amon"
      description: "another markup and object notation language"
      sources:
      - name: "gitlab"
        url: "https://gitlab.com/4bcx/amon"
      - name: "github"
        url: "https://github.com/4bcx/amon"
      license:
        name: "The Unlicense"
        url: "https://unlicense.org"

    - name: "esht"
      description: "embeddable shell tags parser thingie"
      sources:
      - name: "gitlab"
        url: "https://gitlab.com/4bcx/esht"
      - name: "github"
        url: "https://github.com/4bcx/esht"
      license:
        name: "The Unlicense"
        url: "https://unlicense.org"

---

the complete list of stuff I worked on