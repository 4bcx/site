# site - 4b.cx

## Description
This is source for my personal website, accessible through [4b.cx](https://4b.cx/) and hosted on [GitLab pages](https://pages.gitlab.io/).

## Credits

- Built using [Zola](https://www.getzola.org/) static site generator
- Style is based on [riggraz](https://riggraz.dev/)'s [no-style-please](https://riggraz.dev/no-style-please/) theme
- Main font used here is [Alexandria](https://fonts.google.com/specimen/Alexandria) by the amazing [Mohamed Gaber](https://gaber.design/) and is licensed under [OFL](https://github.com/Gue3bara/Alexandria/blob/master/OFL.txt)
- `.gitignore` file was generated using [gitignore.io](https://www.toptal.com/developers/gitignore)
- Slides utilize [Hakim El Hattab](https://hakim.se/)'s [reveal.js](https://revealjs.com/), The HTML presentation framework

## License

- Code for this site is [MIT](https://gitlab.com/4bcx/site/-/blob/pages/licenses/MIT)
- Content is [CC-BY-SA](https://gitlab.com/4bcx/site/-/blob/pages/licenses/CC-BY-SA)
- Logo is [CC-NC-ND](https://gitlab.com/4bcx/site/-/blob/pages/licenses/CC-NC-ND)