---
title: "following"
description: "Cool people I follow"
---

- Khaled Hosny &rarr; [website](https://aliftype.com/) / [github](https://github.com/khaledhosny)
- Brad Taunt &rarr; [website](https://bt.ht/) / [sourcehut](https://sr.ht/~bt/)
- Daniel Eklöf &rarr; [github](https://github.com/dnkl) / [codeberg](https://codeberg.org/dnkl)
- Greg Davill &rarr; [website](https://gregdavill.com/) / [github](https://github.com/gregdavill)
- Debra *(GeekMomProjects)* &rarr; [website](https://www.geekmomprojects.com/) / [mastodon](https://mastodon.social/@geekmomprojects)
- Marcus Hutchins *(MalwareTech)* &rarr; [website](https://escapingtech.com/) / [mastodon](https://infosec.exchange/@malwaretech)
- Lesley Carhart &rarr; [website](https://tisiphone.net/) / [mastodon](https://infosec.exchange/@hacks4pancakes)
- Piotr Esden-Tempski &rarr; [website](https://1bitsquared.com/) / [mastodon](https://chaos.social/@esden)
- Simon Ser &rarr; [website](https://emersion.fr/) / [sourcehut](https://sr.ht/~emersion/)
