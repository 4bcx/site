---
title: "kin"
description: "People close to me"
taxonomies:
    tags:
    - family
---

meet my family

## she who must be obeyed

in the realm of our household, she is known by many names; the boss (المدير), mother of boys (أم العيال), she who must be obeyed. but her true name is Esraa (إسراء), not إسرا, never Israa if you like your head attached to your body[^1].

jokes aside, she's an awesome hardworking web developer. you can check her personal portfolio on [esraa.io](https://esraa.io/), and her little-soon-to-be-big company [webmaco](https://webmaco.net/). and together, we made those

### the little one

- **codename:** Zaghloul (زغلول)
- **real name:** Yahya (يحيى)
- **age ATTOW:** 3 yo

my little copy, the heir to my throne. started using github when he was 18 months old, gutted his first computer at the age of two. his hobbies include terrorizing his aunt, manipulating his grandparents to do his bidding, and leaving bits and pieces of his toys for me to step on.

### the littler one

- **codename:** Naanaa (نعناعة)
- **real name:** Yunus (يونس)
- **age ATTOW:** 1 mo

when he was born, he was the loudest child in the hospital. our little burp machine. he is a scorpio like me and he is also our whiter kid.

## the bro

last but not least, my dude, my man, the one and only, bro. he is أكتر واحد أخويا. my partner in many crimes, and my coffee mate (not the creamer thing, I mean he is the one I have coffee with the most). Mahmoud is a marketing wiz, he is the Director of MarkOps[^2] at [Sliq](https://sliqbydesign.com/). you can check his [linkedin](https://www.linkedin.com/in/mahmoud-m-alaa/), he posts a lot of work samples there.

---

[^1]: seriously don't

[^2]: I honestly don't know what that actually means