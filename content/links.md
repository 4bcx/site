---
title: "links"
description: "List of usefull links and stuff to read"
---

## 2022

- [Using your own domain - Masto Guide](https://guide.toot.as/guide/use-your-own-domain/)
- [A successful Git branching model » nvie.com](https://nvie.com/posts/a-successful-git-branching-model/)
- [Developer Certificate of Origin](https://developercertificate.org/)
- [DamZiobro/vim-ide: VIM configured as powerful IDE (Integrated Development Environment)](https://github.com/DamZiobro/vim-ide)
- [40 Linux Server Hardening Security Tips [2022 edition] - nixCraft](https://www.cyberciti.biz/tips/linux-security.html)
- [ABENICS: Active Ball Joint Mechanism With Three-DoF Based on Spherical Gear Meshings | IEEE Journals & Magazine | IEEE Xplore](https://ieeexplore.ieee.org/document/9415699)
- [Using better CLIs - DEV Community 👩‍💻👨‍💻](https://dev.to/sobolevn/using-better-clis-6o8)
- [Ten Things I Wish I’d Known About bash – zwischenzugs](https://zwischenzugs.com/2018/01/06/ten-things-i-wish-id-known-about-bash/)
- [Webring](https://webring.xxiivv.com/)
- [Learn to use email with git!](https://git-send-email.io/)
- [Learn to change history with git rebase!](https://git-rebase.io/)
- [GitHub does dotfiles - dotfiles.github.io](https://dotfiles.github.io/)
- [Complete Checklist for Website Testing](http://www.softwaretestingclass.com/complete-checklist-for-website-testing/)
- [awesome-selfhosted/awesome-selfhosted: A list of Free Software network services and web applications which can be hosted on your own servers](https://github.com/awesome-selfhosted/awesome-selfhosted)
- [Fold 'N Fly » Paper Airplane Folding Instructions](https://www.foldnfly.com/)
- [Instaparse Live](http://instaparse.mojombo.com/)
- [No More Google](https://nomoregoogle.com/)
- [abnfgen - ABNF-grammar test case generator](https://www.quut.com/abnfgen/)


## old stuff I don't remember when I saved them

- [WFH Reason Generator](https://idontlike.work/#/)
- [BCN3D Technologies](https://github.com/BCN3D)
- [10 Ways to Destroy an Arduino — Rugged CircuitsRugged Industrial Arduino Microcontrollers](https://www.rugged-circuits.com/10-ways-to-destroy-an-arduino/)
- [Create GnuPG key with sub-keys to sign, encrypt, authenticate - Experiencing Technology](https://blog.tinned-software.net/create-gnupg-key-with-sub-keys-to-sign-encrypt-authenticate/)
