---
# Hello!
---

- info
    - [about](/about) me and stuff
    - reach me via [email](mailto:aa@4b.cx)
    - <a rel="me" href="https://masto.ai/@4bcx">mastodon</a>

* stuff to checkout
    * [cool people](/following) I follow
    * other [bookmarks](/links)
    * my literal [idea dump](/ideas)