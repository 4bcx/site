---
title: "card project idea"
description: "Idea dump for the virtual business card thing project"
extra:
    image: "writings/card-project-idea/card-screenshot.png"
---

This should be a more organized write down of this Google Keep note I wrote almost a month ago

![card project idea google keep note](card-screenshot.png)

I thought of this while attending a conference here in Alexandria; what if there is a way for people to exchange contact information, not only giving them. I mean if I gave someone my phone number, I should have theirs, and I should know where we met and *maybe* what we talked about. I know there are [tons of similar projects and products](https://samanthabrandon.com/best-digital-business-card), but I've settled on some features that *I think* will put this project in a different scope.

Also before I start rambling, I'm making this to solve my own problem of not remembering who calls me and being too lazy to save each and every one I meet in a conference or a talk.

## the bullet-points

- the final project MUST be portable and self-hostable
    - in php for maximum self-hostability
    - export vcards and csv files
    - flat-file first *or only*
    - web-finger-like discovery
- users can have multiple profiles/documents with defaults, for example:
    - personal contact information as default
    - different work business cards for freelancers/contractors
    - documents like resumes and portfolios
- different permission levels, for example:
    - in my personal vcard, only website and social media are public, email is available for exchange (when someone shares their name and email address with me), and phone number available for exchange after my approval (when someone shares their name and phone number and I accept their request)
    - my work business card is available for exchange (name, email address, organization and position)
    - my resume (a document) is available under the same conditions as my work business card
- sharing links are taggable
- I should be able to export a list of contacts
- **most important:** one click contacts exchange with other instances
- other fancy features
    - appointment scheduling page with CalDav export
    - contacts synchronization using CardDav
    - themes and layouts for public pages

What I picture is this; I give people a link that has my public contacts and a button called "exchange contacts" for example. When someone clicks this button one of two scenarios should happen:

- if they dont have an instance of this project, a simple form asking for whatever information I want for exchange
    - a cookie should tell me if they submitted the form before
    - also the server should verify that there are no duplicates
    - a vcard is downloaded
- if they have an instance, a magical thing I didn't figure out yet should happen and my information is copied to their list and their information is copied to mine

POC for the first scenario is WIP. Still researching the other part.

## todo

- [ ] finalize the whole idea
- [ ] produce POC