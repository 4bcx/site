---
title: "pictures"
description: "Mainly a log of places I visited"
sort_by: "date"
---

This is like a photolog of family trips, photos during commute or other stuff I find interesting. Entries may or may not contain context or description.