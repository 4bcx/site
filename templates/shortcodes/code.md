{% set path = page.relative_path | replace(from="index.md", to=file) %}
{% set code = load_data(path=path) %}
<p style="margin-bottom:-0.75rem; font-style:oblique; text-align:right;">file: <a style="text-decoration:none;" href="{{ page.path ~ file }}">{{ file }}</a></p>

```linenos{% if highlight %},{{ highlight }}{% endif %}
{{ code }}
```