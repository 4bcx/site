#!/bin/bash

PROJECTS_DIR="$( dirname $PWD )"
AUTHOR_EMAIL="$( git config --get --global user.email )"
OUTPUT_FILE="$( realpath static/images/git_calendar.svg )"

TEMP_FILE="$( mktemp )"

for REPO in $( find "${PROJECTS_DIR}" \
    -type d -name '.git' -printf '%h\n' )
do
    cd "${REPO}"
    git log --all --format=tformat:%as --author="${AUTHOR_EMAIL}" \
        >> "${TEMP_FILE}"
done

cd "${PROJECTS_DIR}"

cat << SVG_BEGIN_BLOCK > "${OUTPUT_FILE}"
<svg width="840" height="168" version="1.1" viewBox="0 0 840 168" xmlns="http://www.w3.org/2000/svg">
<style>
    .dt { font: 12px monospace }
    .sq { width: 12px; height: 12px; rx: 2px; ry: 2px; }
    .f0 { fill: rgb(245,245,255); }
    .f1 { fill: rgb(205,205,255); }
    .f2 { fill: rgb(155,155,255); }
    .f3 { fill: rgb(105,105,255); }
    .f4 { fill: rgb(55,55,255); }
    .f5 { fill: rgb(5,5,255); }
</style>
<text x="20" y="60" class="dt">Sun</text>
<text x="20" y="88" class="dt">Tue</text>
<text x="20" y="116" class="dt">Thu</text>
SVG_BEGIN_BLOCK

TODAY="$( date +'%w:%a, %d %B %Y' )"
LASTYEAR="$( date -d 'last year' +'%w:%a, %d %B %Y' )"
TOMORROW="$( date -d 'tomorrow' +'%w:%a, %d %B %Y' )"

DATE_POINTER="${LASTYEAR}"
MONTH_TAG=""
COLUMN=0
while [ "${DATE_POINTER}" != "${TOMORROW}" ]; do
    ROW=$(( ( ${DATE_POINTER%:*} + 1 ) % 7 ))
    MONTH="$( date -d "${DATE_POINTER#*:}" +'%b' )"
    if [ $ROW -eq 0 ] && [ "${MONTH_TAG}" != "${MONTH}" ]; then
        MONTH_TAG="${MONTH}"
        printf '<text x="%s" y="32" class="dt">%s</text>' \
            "$(( ( COLUMN * 14 ) + 48 ))" \
            "${MONTH_TAG}" >> "${OUTPUT_FILE}"
    fi
    printf '<rect class="sq %s" x="%s" y="%s"><title>%s commits on %s</title></rect>\n' \
        "f0" \
        "$(( ( COLUMN * 14 ) + 48 ))" \
        "$(( ( ROW * 14 ) + 36 ))" \
        "No" \
        "${DATE_POINTER#*:}" >> "${OUTPUT_FILE}"
    [ $ROW -eq 6 ] && (( COLUMN++ ))
    DATE_POINTER="$( date -d "${DATE_POINTER#*:} + 1 day" +'%w:%a, %d %B %Y' )"
done

sort "${TEMP_FILE}" | uniq -c | while read COMMITS DATE; do
LEVEL=$(( ( COMMITS / 5 ) + 1 ))
[ ${LEVEL} -gt 5 ] && LEVEL=5
LEVEL="f${LEVEL}"
DATE_STR="$( date -d "${DATE}" +'%a, %d %B %Y' )"
sed -i -E "s/(^.*)(f0)(.*)(No)(.*)(${DATE_STR})(.*$)/\1${LEVEL}\3${COMMITS}\5\6\7/" "${OUTPUT_FILE}"
done

cat << SVG_END_BLOCK >> "${OUTPUT_FILE}"
<text x="20" y="150" class="dt">Generated ${TODAY#*:}</text>
<text x="480" y="150" class="dt">Hover on the boxes for details</text>
<rect class="sq f0" x="720" y="140"><title>No commits</title></rect>
<rect class="sq f1" x="734" y="140"><title>1 to 5 commits</title></rect>
<rect class="sq f2" x="748" y="140"><title>6 to 10 commits</title></rect>
<rect class="sq f3" x="762" y="140"><title>11 to 15 commits</title></rect>
<rect class="sq f4" x="776" y="140"><title>15 to 20 commits</title></rect>
<rect class="sq f5" x="790" y="140"><title>21+ commits</title></rect>
</svg>
SVG_END_BLOCK

rm "${TEMP_FILE}"
