---
title: "ideas"
description: "List of ideas that may or may not come true"
---

This is a list of ideas and projects that may or may not come true

- ~~**esht:** embeddable shell tags - shell preprocessor~~
- ~~**site:** personal website and blogging system~~
- **oshe:** Egypt's open-source hardware branch & projects repository
- **labs:** technology labs operating system
- **tpcb:** test PCBs
- **fact:** PCB & 3D printing microfactory
- **yaas:** yet another automation solution
- **taps:** typesetting & printing system
- **dash:** database & spreadsheets hybrid
- **venp:** virtual environments & project management
- **sign:** electronic signature & document management system
- **porg:** personal organizer, PIM suit
- **amon:** minimal markup & object notation language
- **gman:** Gerber files manipulator
- **bott:** some bots
- **dweb:** static site generator
- **msho:** community for developers
- **card:** business cards & name tags
- **prnt:** printing & stationary supplier store
- **wesl:** web based STL slicer
- ~~**imgg:** cover images generator~~
- **rope:** string braiding machine for making bracelets
- **hwes:** headless web services
- **toot:** mastodon instance
- **msgr:** email & texting hybrid
- **ossh:** open source software house
- **base:** co-working space & innovation hub
